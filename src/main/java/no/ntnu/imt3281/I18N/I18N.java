
/**
 * 
 */
package no.ntnu.imt3281.I18N;

import java.text.ChoiceFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * All I18N handled through this class.
 * 
 * @author oivindk
 *
 */
public class I18N {
    @SuppressWarnings("unused")
	private static I18N i18n = new I18N();
    private static ResourceBundle bundle;
    private static final String RESOURCEPACKAGE = "no.ntnu.imt3281.I18N.i18n";

    private I18N() {
        I18N.bundle = ResourceBundle.getBundle(I18N.RESOURCEPACKAGE,
                Locale.getDefault());
    }

    /**
     * Allow override of language.
     * 
     * @param language use this as language
     */
    public static void setLanguage(String language) {
        Locale locale = new Locale(language);
        I18N.bundle = ResourceBundle.getBundle(I18N.RESOURCEPACKAGE,
                locale);
    }

    /**
     * Allow override of language and country.
     * 
     * @param language use this as language
     * @param country use this as country
     */
    public static void setLanguageCountry(String language, String country) {
        Locale locale = new Locale(language, country);
        I18N.bundle = ResourceBundle.getBundle(I18N.RESOURCEPACKAGE,
                locale);
    }

    /**
     * Get resource bundle
     * 
     * @return ResourceBundle for application
     */
    public static ResourceBundle getBundle() {
        return bundle;
    }

    /**
     * Gets the string for the given key from the current resource bundle.
     * @see java.util.ResourceBundle.getString(String)
     * 
     * @param key points to the resource to get.
     * @return the value that the key points to.
     */
    public static String getString (String key) {
    	return I18N.bundle.getString(key);
    }
    
    /**
     * Gets a concatenated string. Inserts data from the provided array of
     * objects into the pattern pointed to by the pattern parameter
     * 
     * @param pattern
     *            a string pointing to the pattern to be used
     * @param data
     *            an array of object to insert into a pattern
     * 
     * @return a String where data is inserted into pattern
     */
    public static String getConcatenatedString(String pattern, Object[] data) {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(I18N.bundle.getLocale());
        formatter.applyPattern(I18N.bundle.getString(pattern));
        return formatter.format(data);
    }

    /**
     * Gets a string where parts of it can be of the form (none, one, x). Use pattern as the key for the pattern for the string,
     * the none, one and more is keys to these forms and data is the data to be inserted into pattern.
     * @param pattern a string pointing to the pattern to be used
     * @param none the key for the string to use if 0
     * @param one the key for the string to use if 1
     * @param more the key for the string to use if 2 or more
     * @param data an array holding the data to be inserted into pattern
     * @return a String where data is inserted into the pattern
     */
    public static String getPluralString(String pattern, String none,
            String one, String more, Object[] data) {
        MessageFormat messageForm = new MessageFormat("");
        messageForm.setLocale(I18N.bundle.getLocale());
        double[] limits = { 0, 1, 2 };
        String[] strings = { I18N.bundle.getString(none), I18N.bundle.getString(one),
                I18N.bundle.getString(more) };
        ChoiceFormat choiceForm = new ChoiceFormat(limits, strings);
        messageForm.applyPattern(I18N.bundle.getString(pattern));
        Format[] formats = { choiceForm, null, NumberFormat.getInstance() };
        messageForm.setFormats(formats);
        return messageForm.format(data);
    }
}
