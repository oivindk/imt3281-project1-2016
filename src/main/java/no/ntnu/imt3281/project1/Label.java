package no.ntnu.imt3281.project1;

/**
 * Objects of this class will represent JLabels in the layout created by the editor
 * 
 * @author �ivind Kolloen
 *
 */
public class Label extends BaseComponent {

	/**
	 * Update when adding/removing fields from this class, used for saving/loading to file.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see BaseComponent#BaseComponent(BaseComponent)
	 * @param component base the new label on this component
	 */
	public Label(BaseComponent component) {
		super(component);
	}

	/**
	 * @see BaseComponent#BaseComponent()
	 */
	public Label() {
		super();
	}

	/**
	 * Used to get the definition of the JLabel.
	 * this code will be placed just below the class definition of the generated code.
	 * 
	 * @return a String with the Java code for creating this label as a property in the class.
	 */
	@Override
	public String getDefinition() {
		return "\tJLabel "+getVariableName()+" = new JLabel(\""+getText()+"\");\n";
	}
}
