package no.ntnu.imt3281.project1;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import no.ntnu.imt3281.I18N.I18N;
import no.ntnu.imt3281.help.Help;

/**
 * This class contains the main method and initialize the whole application.
 *
 */
@SuppressWarnings("serial")
public class App extends JFrame {

	private static final int UP = 1;
	private static final int DOWN = -1;
	private GBLEDataModel data = new GBLEDataModel();
	private JTable table = new JTable(data);
	private File savedAsFile = null;
	private boolean changedSinceLastSave = false;
	transient Logger logger = Logger.getLogger(getClass().getName());
	private Help helpWindow = new Help();
	
	public App() {
		super(I18N.getString("app.title"));
		setTableEditorsAndRenderers(table);
		add(new JScrollPane(table));
		JToolBar toolbar = new JToolBar();
		JMenuBar menubar = new JMenuBar();
		JPopupMenu popupMenuWithSpecialEditor = new JPopupMenu();
		JPopupMenu popupMenuWithoutSpecialEditor = new JPopupMenu();
		fillFileMenuItems(menubar, toolbar);
		fillEditMenuItems(menubar, toolbar, popupMenuWithoutSpecialEditor, popupMenuWithSpecialEditor);
		fillHelpMenuItems(menubar, toolbar);
		add(toolbar, BorderLayout.NORTH);
		addPopupMenuToTable(popupMenuWithoutSpecialEditor, popupMenuWithSpecialEditor);
		setJMenuBar(menubar);
		setSize(850, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// Creates editors and renderers for the different columns of the table
	private void setTableEditorsAndRenderers(JTable table) {
		// Add editor for component type column
		String[] componentTypes = { "JLabel", "JButton", "JTextField", "JTextArea" };
		JComboBox<String> componentTypeEditor = new JComboBox<>(componentTypes);
		table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(componentTypeEditor));

		// Add editor (with custom renderer) and custom renderer for anchor column
		AnchorScaleEditorRenderer anchorScaleEditorRenderer = new AnchorScaleEditorRenderer();
		JComboBox<Integer> anchorEditor = new JComboBox<>(anchorScaleEditorRenderer.getAlignValues());
		anchorEditor.setRenderer(anchorScaleEditorRenderer);
		table.getColumnModel().getColumn(7).setCellEditor(new DefaultCellEditor(anchorEditor));
		table.getColumnModel().getColumn(7).setCellRenderer(new AnchorScaleCellRenderer(anchorScaleEditorRenderer));
		
		// Add editor (with custom renderer) and custom renderer for scale column
		JComboBox<Integer> scaleEditor = new JComboBox<>(anchorScaleEditorRenderer.getScaleValues());
		scaleEditor.setRenderer(anchorScaleEditorRenderer);
		table.getColumnModel().getColumn(8).setCellEditor(new DefaultCellEditor(scaleEditor));
		table.getColumnModel().getColumn(8).setCellRenderer(new AnchorScaleCellRenderer(anchorScaleEditorRenderer));
		
		// Set row/column dimensions
		table.setRowHeight(20);
		table.getColumnModel().getColumn(0).setPreferredWidth(60);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);
		table.getColumnModel().getColumn(2).setPreferredWidth(180);
		table.getColumnModel().getColumn(3).setPreferredWidth(50);
		table.getColumnModel().getColumn(4).setPreferredWidth(50);
		table.getColumnModel().getColumn(5).setPreferredWidth(50);
		table.getColumnModel().getColumn(6).setPreferredWidth(50);
		table.getColumnModel().getColumn(7).setPreferredWidth(90);
		table.getColumnModel().getColumn(8).setPreferredWidth(90);
	}

	// Creates the File menu, also fills inn file related options in the toolbar
    private void fillFileMenuItems(JMenuBar menubar, JToolBar toolbar) {		
		JMenu file = new JMenu(I18N.getString("menu.file"));

		// New file/model
		ToolbarAndMenuItem newFile = new ToolbarAndMenuItem(e->clear(), "/icons/new.png", "menu.file.new");
		file.add(newFile.getJMenuItem());
		toolbar.add(newFile.getAbstractAction());
		
		file.addSeparator();
		toolbar.addSeparator();
		
		// Load from file
		ToolbarAndMenuItem loadFile = new ToolbarAndMenuItem(e->load(), "/icons/opendoc.png", "menu.file.load");
		file.add(loadFile.getJMenuItem());
		toolbar.add(loadFile.getAbstractAction());

		// Save to file
		ToolbarAndMenuItem saveFile = new ToolbarAndMenuItem(e->save(), "/icons/save.png", "menu.file.save");
		file.add(saveFile.getJMenuItem());
		toolbar.add(saveFile.getAbstractAction());
		
		// Save to file
		ToolbarAndMenuItem saveFileAs = new ToolbarAndMenuItem(e->saveAs(), null, "menu.file.saveAs");
		file.add(saveFileAs.getJMenuItem());
		
		file.addSeparator();
		toolbar.addSeparator();
		
		// Save Java code to file
		ToolbarAndMenuItem saveSource = new ToolbarAndMenuItem(e->saveJavaSource(), "/icons/savejava.png", "menu.file.saveJavaCode");
		file.add(saveSource.getJMenuItem());
		toolbar.add(saveSource.getAbstractAction());
		
		file.addSeparator();
		toolbar.addSeparator();
		
		// Exit
		ToolbarAndMenuItem exit = new ToolbarAndMenuItem(e->{ /* exit */ }, null, "menu.file.exit");
		file.add(exit.getJMenuItem());
		
		menubar.add(file);
	}

	// Creates the Edit menu, also fills inn edit related options in the toolbar and popup menues
    private void fillEditMenuItems(JMenuBar menubar, JToolBar toolbar, JPopupMenu popupMenu, JPopupMenu popupMenu1) {		
		JMenu edit = new JMenu(I18N.getString("menu.edit"));

		// New row
		ToolbarAndMenuItem newRow = new ToolbarAndMenuItem(e->{ data.addComponent(new Label());  changedSinceLastSave = true; }, 
															"/icons/newrow.png", "menu.edit.new");
		edit.add(newRow.getJMenuItem());
		popupMenu.add(newRow.getJMenuItem());
		popupMenu1.add(newRow.getJMenuItem());
		toolbar.add(newRow.getAbstractAction());
				
		// Move row up
		ToolbarAndMenuItem moveRowUp = new ToolbarAndMenuItem(e->moveRow(UP) , "/icons/moverowup.png", "menu.edit.up");
		edit.add(moveRowUp.getJMenuItem());
		popupMenu.add(moveRowUp.getJMenuItem());
		popupMenu1.add(moveRowUp.getJMenuItem());
		toolbar.add(moveRowUp.getAbstractAction());

		// Move row down
		ToolbarAndMenuItem moveRowDown = new ToolbarAndMenuItem(e->moveRow(DOWN), "/icons/moverowdown.png", "menu.edit.down");
		edit.add(moveRowDown.getJMenuItem());
		popupMenu.add(moveRowDown.getJMenuItem());
		popupMenu1.add(moveRowDown.getJMenuItem());
		toolbar.add(moveRowDown.getAbstractAction());
		
		edit.addSeparator();
		popupMenu.addSeparator();
		popupMenu1.addSeparator();

		// Delete row
		ToolbarAndMenuItem deleteRow = new ToolbarAndMenuItem(e->deleteRow(), null, "menu.edit.delete");
		edit.add(deleteRow.getJMenuItem());
		popupMenu.add(deleteRow.getJMenuItem());
		popupMenu1.add(deleteRow.getJMenuItem());
		
		menubar.add(edit);
	}
    
	// Adds popupmenu to the jtable object
	private void addPopupMenuToTable(JPopupMenu popupMenuWithoutSpecialEditor, JPopupMenu popupMenuWithSpecialEditor) {
		popupMenuWithSpecialEditor.addSeparator();
		JMenuItem extraEditor = new JMenuItem(I18N.getString("popupMenu.editExtraProperties"));
		extraEditor.setToolTipText(I18N.getString("popupMenu.editExtraProperties.tooltip"));
		extraEditor.addActionListener(e->showExtraEditor());
		popupMenuWithSpecialEditor.add(extraEditor);
		table.addMouseListener(new PopupMenuListener(popupMenuWithoutSpecialEditor, popupMenuWithSpecialEditor));
	}

	// Move selected row up/down
	private void moveRow(int direction) {
		int row = table.getSelectedRow();
		if (row>-1) {		
			if (direction==UP) {
				data.moveComponentUp(row);
				if (row>0) {	// Row was moved
					table.getSelectionModel().setSelectionInterval(row-1, row-1);	// Update selection, keep component selected
				}
			} else if (direction==DOWN) {
				data.moveComponentDown(row);
				if (row<data.getRowCount()-1) {	// Row was moved
					table.getSelectionModel().setSelectionInterval(row+1, row+1);	// Update selection, keep component selected
				}
			}
		} else {
			JOptionPane.showMessageDialog(this, I18N.getString("error.moveRow.noRowSelected"));
		}
	}
	
	private void deleteRow() {
		int row = table.getSelectedRow();
		if (row>-1) {
			data.removeComponent(row);
			if (row<data.getRowCount()) {	// we where not on last row
				table.getSelectionModel().setSelectionInterval(row, row);	// Update selection, keep same row selected
			} else {
				table.getSelectionModel().setSelectionInterval(row-1, row-1);	// Update selection, select previous row
			}
		} else {
			JOptionPane.showMessageDialog(this, I18N.getString("error.deleteRow.noRowSelected"));
		}
	}

	// Creates the Help menu, also fills inn help related options in the toolbar
    private void fillHelpMenuItems(JMenuBar menubar, JToolBar toolbar) {		
		JMenu help = new JMenu(I18N.getString("menu.help"));

		toolbar.add(Box.createHorizontalGlue());
		
		// New row
		ToolbarAndMenuItem usage = new ToolbarAndMenuItem(e->helpWindow.displayPage("index.html"), "/icons/help.png", "menu.help.usage");
		help.add(usage.getJMenuItem());
		toolbar.add(usage.getAbstractAction());
						
		help.addSeparator();

		// Delete row
		ToolbarAndMenuItem about = new ToolbarAndMenuItem(e->helpWindow.displayPage("about.html"), null, "menu.help.about");
		help.add(about.getJMenuItem());
		
		menubar.add(help);
	}
    
    /**
     * Clear model and start a new one. If there has been changes since last save, ask if the user want to save them
     */
    private void clear() {
    	int optionSelected = JOptionPane.YES_OPTION;
    	if (changedSinceLastSave) {
    		optionSelected = JOptionPane.showConfirmDialog(this, I18N.getString("warning.io.unsavedChangesSaveNow"));
    		if (optionSelected==JOptionPane.YES_OPTION) {
    			save();
    		}
    	}
    	if (optionSelected==JOptionPane.YES_OPTION||optionSelected==JOptionPane.NO_OPTION) {
    		data.clear();
    	}
    }
    
    /**
     * Load an existing model from disc. If there has been changes since last save, ask if the user want to save them first.
     */
    private void load() {
    	int optionSelected = JOptionPane.YES_OPTION;
    	if (changedSinceLastSave) {
    		optionSelected = JOptionPane.showConfirmDialog(this, I18N.getString("warning.io.unsavedChangesSaveNow"));
    		if (optionSelected==JOptionPane.YES_OPTION) {
    			save();
    		}
    	}
    	if (optionSelected==JOptionPane.YES_OPTION||optionSelected==JOptionPane.NO_OPTION) {
    		JFileChooser chooser = new JFileChooser();
    		FileFilter filter = new FileNameExtensionFilter("GBLE layout file", "GBLE");
    		chooser.addChoosableFileFilter(filter);
    		chooser.setAcceptAllFileFilterUsed(false);
    		int retrival = chooser.showOpenDialog(this);
			if (retrival == JFileChooser.APPROVE_OPTION) {
				try {
					FileInputStream fis = new FileInputStream(chooser.getSelectedFile());
					data.load(fis);
					savedAsFile = chooser.getSelectedFile();
					changedSinceLastSave = false;
				} catch (FileNotFoundException e) {
					logger.log(Level.OFF, "Unable to load file", e);
					JOptionPane.showMessageDialog(this, I18N.getString("error.io.load"));
				}
			}
    	}
    }

    /**
     * Saves content of the model to a new file
     */
	private void saveAs() {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(I18N.getString("saveDialog.title"));
		FileFilter filter = new FileNameExtensionFilter("GBLE layout file", "GBLE");
		chooser.addChoosableFileFilter(filter);
		chooser.setAcceptAllFileFilterUsed(false);
		int retrival;
		do {
			retrival = chooser.showSaveDialog(this);
			if (retrival == JFileChooser.APPROVE_OPTION && Character.isLowerCase(chooser.getSelectedFile().getName().charAt(0))) {
				JOptionPane.showMessageDialog(this, I18N.getString("error.io.lowerCaseFileName"));
			}
		} while (retrival == JFileChooser.APPROVE_OPTION && Character.isLowerCase(chooser.getSelectedFile().getName().charAt(0)));
	    if (retrival == JFileChooser.APPROVE_OPTION) {
	        try {
	        	File f = chooser.getSelectedFile();
	        	int idx = f.getName().indexOf('.');
	        	if (idx>0) {
	        		renameFile(f);
	        	} else {
	        		f.renameTo(new File(f+".GBLE"));
	        	}
	        	FileOutputStream fos = new FileOutputStream(f);
	        	data.save(fos);
	        	fos.close();
	        	savedAsFile = f;
	        	changedSinceLastSave = false;
	        } catch (IOException ex) {
	        	logger.log(Level.OFF, "Unable to save file", ex);
	        	JOptionPane.showMessageDialog(this, I18N.getString("error.io.filesave"));
	        }
	    }
	}

	/**
	 * Ask user if he want to change file extension to comply with GBLE file extension
	 * @param f
	 */
	private void renameFile(File f) {
		if (!f.getName().endsWith(".GBLE")) {
			JOptionPane.showConfirmDialog(this, I18N.getString("warning.io.renameToGBLE"));
		}
	}
	
	/**
     * Saves content of the model to last used file
     */
	private void save() {
		if (savedAsFile!=null) {
	        try {
	        	FileOutputStream fos = new FileOutputStream(savedAsFile);
	        	data.save(fos);
	        	fos.close();
	        	changedSinceLastSave = false;
	        } catch (IOException ex) {
	        	logger.log(Level.OFF, "Unable to save file", ex);
	        	JOptionPane.showMessageDialog(this, I18N.getString("error.io.filesave"));
	        }
	    } else {
	    	saveAs();
	    }
	}

	/**
     * Saves content of the model as Java source code to .java file.
     */
	private void saveJavaSource() {
		if (savedAsFile==null) {	// If file not saved, ask the user to save the file
	    	saveAs();
	    }
		if (savedAsFile!=null) {	// If the user saved the file, write Java source code
			String className = savedAsFile.getName().substring(0, savedAsFile.getName().lastIndexOf('.'));
			StringBuilder sb = new StringBuilder();
			sb.append("import java.awt.*;\n");
			sb.append("import javax.swing.*;\n\n");
			sb.append("public class ");
			sb.append(className);
			sb.append(" extends JPanel {\n");
			sb.append(data.getDefinitions());
			sb.append("\n");
			sb.append("\tpublic ");
			sb.append(className);
			sb.append("() {\n");
			sb.append("\t\tGridBagLayout layout = new GridBagLayout();\n");
			sb.append("\t\tGridBagConstraints gbc = new GridBagConstraints();\n");
			sb.append(data.getLayoutCode());
			sb.append("\t}\n");
			sb.append("\n\tpublic static void main(String args[]) {\n");
			sb.append("\t\tJFrame testWindow = new JFrame(\"Test window\");");
			sb.append("\t\ttestWindow.add(new ");
			sb.append(className);
			sb.append(" ());\n");
			sb.append("\t\ttestWindow.pack();\n");
			sb.append("\t\ttestWindow.setVisible(true);\n");
			sb.append("\t\ttestWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);\n");
			sb.append("\t}\n");
			sb.append("}");
			BufferedWriter bw;
			try {
				bw = new BufferedWriter(new FileWriter(savedAsFile.getParent()+File.separator+className+".java"));
				bw.write(sb.toString());
				bw.close();
			} catch (IOException e) {
				logger.log(Level.OFF, "Error saving source code to file", e);
				JOptionPane.showMessageDialog(this, I18N.getString("error.io.saveSourceCode"));
			}
		}
	}

	// Get the editorPanel for the component at the selected row and show it in a dialog
	private void showExtraEditor() {
		JPanel editorPanel = (JPanel)data.getValueAt(table.getSelectedRow(), 9);
		JDialog editor = new JDialog(this, I18N.getString("extraEditor.title"));
		editor.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		editor.setResizable(false);
		editor.add(editorPanel);
		editor.setModal(true);
		editor.pack();
		editor.setVisible(true);
	}


	public static void main( String[] args ) {
        App app = new App();
        app.setVisible(true);
    }
}