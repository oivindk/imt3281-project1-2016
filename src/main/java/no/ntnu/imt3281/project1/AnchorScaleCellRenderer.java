package no.ntnu.imt3281.project1;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Used to render values in the anchor column in the GBLE table view
 * @author oivindk
 *
 */
@SuppressWarnings("serial")
public class AnchorScaleCellRenderer extends JLabel implements TableCellRenderer {
	AnchorScaleEditorRenderer editorRenderer = null;
	
	/**
	 * Must store a reference to a AnchorEditorRenderer to get to the map to convert between integers and strings.
	 * @see AnchorScaleEditorRenderer#getAlignMap()
	 * 
	 * @param anchorEditorRenderer reference to a AnchorEditorRenderer
	 */
	public AnchorScaleCellRenderer(AnchorScaleEditorRenderer anchorEditorRenderer) {
		editorRenderer = anchorEditorRenderer;
		setOpaque(true);
	}

	/**
	 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(JTable, Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		if (isSelected) {
			setBackground(table.getSelectionBackground());
		} else {
			setBackground(table.getBackground());
		}
		setIcon(new ImageIcon(getClass().getResource("/icons/"+editorRenderer.getMap().get(value)+".png")));
		return this;
	}
}
