package no.ntnu.imt3281.project1;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;
import javax.swing.JTable;

class PopupMenuListener extends MouseAdapter {
	JPopupMenu popupMenuWithoutSpecialEditor;
	JPopupMenu popupMenuWithSpecialEditor;
	
	public PopupMenuListener(JPopupMenu popupMenuWithoutSpecialEditor, JPopupMenu popupMenuWithSpecialEditor) {
		this.popupMenuWithoutSpecialEditor = popupMenuWithoutSpecialEditor;
		this.popupMenuWithSpecialEditor = popupMenuWithSpecialEditor;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		checkForTrigger(e);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		checkForTrigger(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		checkForTrigger(e);
	}

	private void checkForTrigger(MouseEvent e) {
		if (e.isPopupTrigger()) {
	        JTable source = (JTable)e.getSource();
	        int row = source.rowAtPoint( e.getPoint() );

	        if (source.getSelectedRow()<0) {
	            source.changeSelection(row, 0, false, false);
	        }

	        if (source.getSelectedRow()>-1) {
	        	Component specialEditor = (Component) source.getModel().getValueAt(row, 9);
	            if (specialEditor==null) {
	            	popupMenuWithoutSpecialEditor.show(e.getComponent(), e.getX(), e.getY());
	            } else {
	            	popupMenuWithSpecialEditor.show(e.getComponent(), e.getX(), e.getY());
	            }
	        }
	    }
	}
}
