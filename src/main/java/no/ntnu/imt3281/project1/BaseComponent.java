package no.ntnu.imt3281.project1;

import java.awt.Component;
import java.io.Serializable;

/**
 * This class contains the functions and properties that are common for all components
 * 
 * @author Øivind Kolloen
 *
 */
public class BaseComponent implements Serializable {
	/**
	 * Update this when adding/removing fields from this class, used for saving/loading layouts to file.
	 */
	private static final long serialVersionUID = 1L;
	private String variableName;
	private String text="";
	protected static int nextComponentID=1;
	private int col=1;
	private int row=1;
	private int cols=1;
	private int rows=1;
	private int anchor=java.awt.GridBagConstraints.CENTER;
	private int fill=java.awt.GridBagConstraints.NONE;
	
	

	/**
	 * Creates a new component, initialize the component variable name.
	 * Sets the variable name for the component to "component" and append the static nextComponentID variable
	 * that gets incremented each time a new component is created. 
	 */
	public BaseComponent() {
		variableName = "component"+nextComponentID;
		nextComponentID++;
	}
	
	/**
	 * Create a new component, take all standard values from the component passed in as a parameter.
	 * 
	 * @param component create a new component based on this component. I.e. use all standard values from this component.
	 */
	public BaseComponent(BaseComponent component) {
		this.variableName = component.variableName;
		this.text = component.text;
		this.col = component.col;
		this.row = component.row;
		this.cols = component.cols;
		this.rows = component.rows;
		this.anchor = component.anchor;
		this.fill = component.fill;
	}

	/**
	 * Returns the variable name for the component.
	 * 
	 * @return a String with the variable name for this component
	 */
	public String getVariableName() {
		return variableName;
	}

	/**
	 * Return the text that is contained in this component.
	 * 
	 * @return a String which is the content of the label, textfield, button or whatever this component actually is.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Classes that represents components that has special fields must override this method to provide an 
	 * editor for those fields.
	 * Components that has fields that do not fit in the regular table view of component properties must provide 
	 * a component that acts as a editor for those fields. 
	 * 
	 * @return a JPanel to be shown as editor for special fields. null for components that do not have special fields.
	 */
	public Component getSpecialEditor() {
		return null;
	}

	/**
	 * Used to get the column that this component should be placed in.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @return an int that gives the column for this component.
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Used to get the row that this component should be placed in.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @return an int that gives the row for this component.
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Used to get the number of columns that this component should spawn.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @return an int that gives the number of columns that this component should spawn.
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Used to get the number of rows that this component should spawn.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @return an int that gives the number of rows that this component should spawn.
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Used to get the anchoring for this component.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @return an int that gives the anchoring for this component.
	 */
	public int getAnchor() {
		return anchor;
	}
	
	/**
	 * Used to get the fill mode for this component.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @return an int that gives the fill mode for this component.
	 */
	public int getFill() {
		return fill;
	}

	/**
	 * Set the text for this component. The text of the component is the label show, the text in a textfield etc.
	 * 
	 * @param text what should be the text for this component.
	 */
	public void setText(String text) {
		this.text = text;		
	}

	/**
	 * Set the variable name for this component.
	 * 
	 * @param variableName the new variable name for this component.
	 */
	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	/**
	 * Set the column that this component should be placed in.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @param col the column that this component should be placed in.
	 */
	public void setCol(int col) {
		this.col = col;		
	}
	
	/**
	 * Set the row that this component should be placed in.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @param row the row that this component should be placed in.
	 */
	public void setRow(int row) {
		this.row = row;		
	}
	
	/**
	 * Set the number of columns that this component should occupy.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @param cols the number of columns that this component should occupy.
	 */
	public void setCols(int cols) {
		this.cols = cols;		
	}
	
	/**
	 * Set the number of rows that this component should occupy. 
	 * @see java.awt.GridBagConstraints
	 * 
	 * @param rows the number of rows that this component should occupy.
	 */
	public void setRows(int rows) {
		this.rows = rows;		
	}

	/**
	 * Set the anchoring for this component.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @param anchor the anchoring for this component.
	 */
	public void setAnchor(int anchor) {
		this.anchor = anchor;
	}

	/**
	 * Set the fill mode for this component.
	 * @see java.awt.GridBagConstraints
	 * 
	 * @param fill the fill mode for this component. 
	 */
	public void setFill(int fill) {
		this.fill = fill;
	}

	public String getDefinition() {
		return null;
	};
	
	/**
	 * Return the Java code to be used to place this component in the layout.
	 * This is the code to placed in the constructor of the class that will be created for the complete layout.
	 * 
	 * @return a String with the Java code handling the layout for this component.
	 */
	public String getLayoutCode() {
		StringBuilder sb = new StringBuilder();
		sb.append("\t\tgbc.gridx = ");
		sb.append(col);
		sb.append(";\n\t\tgbc.gridy = ");
		sb.append(row);
		sb.append(";\n\t\tgbc.gridwidth = ");
		sb.append(cols);
		sb.append(";\n\t\tgbc.gridheight = ");
		sb.append(rows);
		sb.append(";\n\t\tgbc.anchor = ");
		sb.append(anchor);
		sb.append(";\n\t\tgbc.fill = ");
		sb.append(fill);
		sb.append(";\n\t\tlayout.setConstraints(");
		sb.append(variableName);
		sb.append(", gbc);");
		sb.append("\n\t\tadd(");
		sb.append(variableName);
		sb.append(");\n");
		return sb.toString();
	}
}
