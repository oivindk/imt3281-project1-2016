package no.ntnu.imt3281.project1;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.ntnu.imt3281.I18N.I18N;

/**
 * Objects of this class will represent JTextFields in the layout created by the editor
 * 
 * @author �ivind Kolloen
 *
 */
public class TextField extends BaseComponent {
	private int width=0;
	transient Logger logger = Logger.getLogger(getClass().getName());

	/**
	 * Update when adding/removing fields from this class, used for saving/loading to file.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see BaseComponent#BaseComponent(BaseComponent)
	 * @param component base the new button on this component
	 */
	public TextField(BaseComponent component) {
		super(component);
	}

	/**
	 * @see BaseComponent#BaseComponent()
	 */
	public TextField() {
		super();
	}

	/**
	 * Set the width of this text field, that is, how many characters wide it should be. 
	 * @see javax.swing.JTextField#setColumns(int)
	 * 
	 * @param width the number of characters this text field should have room for.
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Get the width of this text field, that is, how many characters wide it should be. 
	 * @see javax.swing.JTextField#getColumnWidth(int)
	 * 
	 * @param width the number of characters this text field should have room for.
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Used to get the definition of the JTextField.
	 * This code will be placed just below the class definition of the generated code.
	 * 
	 * @return a String with the Java code for creating this text field as a property in the class.
	 */
	@Override
	public String getDefinition() {
		return "\tJTextField "+getVariableName()+" = new JTextField(\""+getText()+"\""+(width>0?", "+width:"")+");\n";
	}
	
	/**
	 * Return a JDialog used to change the width (the number of characters) of this text field.
	 * @see BaseComponent#getSpecialEditor()
	 *  
	 * @return a JDialog that enables the user to change the desired fields
	 */
	@Override
	public Component getSpecialEditor() {
		JPanel editor = new JPanel ();
		editor.setLayout(new GridLayout(2, 1));
		JPanel edit = new JPanel (new FlowLayout(FlowLayout.LEADING));
		edit.add(new JLabel(I18N.getString("textField.editor.width.label")));
		JTextField widthTF = new JTextField(Integer.toString(width), 10);
		widthTF.setToolTipText("textField.editor.width.tooltip");
		edit.add(widthTF);
		editor.add(edit);
		JPanel buttons = new JPanel();
		JButton ok = new JButton(I18N.getString("button.ok"));
		ok.addActionListener(e->{
			try {
				width = Integer.parseInt(widthTF.getText());
			} catch (RuntimeException re) {
				logger.log(Level.OFF, "User input error", re);
				JOptionPane.showMessageDialog(null, I18N.getString("error.textfield.editor.notANumber"));
				return;
			}
			closeEditor(editor);
		});
		JButton cancel = new JButton(I18N.getString("button.cancel"));
		cancel.addActionListener(e->{
			closeEditor(editor);
		});
		buttons.add(ok);
		buttons.add(cancel);
		editor.add(buttons);
		
		return editor;
	}

	/**
	 * @param editor
	 */
	private void closeEditor(JPanel editor) {
		Component c = editor;
		do {
			c = c.getParent();
		} while (!(c instanceof JDialog));
		((JDialog)c).setVisible(false);
		((JDialog)c).dispose();
	}
}
