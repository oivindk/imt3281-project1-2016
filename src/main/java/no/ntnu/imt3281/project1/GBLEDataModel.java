/**
 * 
 */
package no.ntnu.imt3281.project1;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.table.AbstractTableModel;

import no.ntnu.imt3281.I18N.I18N;

/**
 * @author oivindk
 *
 */
public class GBLEDataModel extends AbstractTableModel {
	/**
	 * Update when adding/removing fields from this class, used for saving/loading to file.
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<BaseComponent> data = new ArrayList<>();
	private String[] columnNames = { I18N.getString("tableTitles.type"), I18N.getString("tableTitles.name"), I18N.getString("tableTitles.text"), I18N.getString("tableTitles.row"), I18N.getString("tableTitles.column"), I18N.getString("tableTitles.rows"), I18N.getString("tableTitles.columns"), I18N.getString("tableTitles.anchor"), I18N.getString("tableTitles.fill") };
	transient Logger logger = Logger.getLogger(getClass().getName());

	/**
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return data.size();
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/**
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int idx) {
		return columnNames[idx];
	}
	
	/**
	 * All cells should be editable, so return true for all rows/columns for isCellEditable
	 * 
	 * @see javax.swing.table.AbstractTableModel#isCellEditable
	 * @param rowIndex the row 
	 * @param columnIndex the column
	 * @return true if cell is editable
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	/**
	 * Get the value of a given cell in the table.
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		BaseComponent c = data.get(rowIndex);
		switch (columnIndex) {
		case 0: return componentClassToString(c);
		case 1: return c.getVariableName();
		case 2: return c.getText();
		case 3: return c.getRow();
		case 4: return c.getCol();
		case 5: return c.getRows();
		case 6: return c.getCols();
		case 7: return c.getAnchor();
		case 8: return c.getFill();
		case 9: return c.getSpecialEditor();
		default: return null;
		}
	}

	/**
	 * set the value of a given cell in the table. This should also handle changing component type if the value
	 * in the first column changes.
	 * 
	 * @see javax.swing.table.TableModel#setValueAt(object, int, int)
	 */
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		BaseComponent c = data.get(rowIndex);
		switch (columnIndex) {
		case 0: changeComponentType(c, rowIndex, value.toString());
				break;
		case 1: c.setVariableName(value.toString());
				break;
		case 2: c.setText(value.toString());
				break;
		case 3: c.setRow(Integer.parseInt(value.toString()));
				break;
		case 4: c.setCol(Integer.parseInt(value.toString()));
				break;
		case 5: c.setRows(Integer.parseInt(value.toString()));
				break;
		case 6: c.setCols(Integer.parseInt(value.toString()));
				break;
		case 7: c.setAnchor((Integer)value);
				break;
		case 8: c.setFill((Integer)value);
		}
		fireTableRowsUpdated(rowIndex, rowIndex);
	}
	
	private void changeComponentType(BaseComponent c, int rowIndex, String value) {
		switch (value) {
		case "JLabel": data.set(rowIndex, new Label(c));
						break;
		case "JButton": data.set(rowIndex, new Button(c));
						break;
		case "JTextField": data.set(rowIndex, new TextField(c));
						break;
		case "JTextArea": data.set(rowIndex, new TextArea(c));
		}
	}

	/**
	 * Get an object of a class inheriting from BaseComponent, return the corresponding swing class as a String.
	 * 
	 * @param c the object to find the Swing class for.
	 * @return a String with the name of the Swing class that this component represents.
	 */
	private Object componentClassToString(BaseComponent c) {
		if (c instanceof Label)
			return "JLabel";
		else if (c instanceof Button)
			return "JButton";
		else if (c instanceof TextField)
			return "JTextField";
		else if (c instanceof TextArea)
			return "JTextArea";
		else
			return "Unknown";
	}

	/**
	 * Adds a new component to the end of this table.
	 * Adds the given component to the end of the table.
	 * 
	 * @param component the component to add.
	 */
	public void addComponent(BaseComponent component) {
		data.add(component);
		fireTableRowsInserted(data.size(), data.size());
	}

	/** 
	 * Remove the component at the given index.
	 * 
	 * @param idx the index of the component to remove.
	 */
	public void removeComponent(int idx) {
		data.remove(idx);
		fireTableRowsDeleted(idx, idx);
	}

	/**
	 * Remove the given component.
	 * 
	 * @param component the component to remove
	 */
	public void removeComponent(BaseComponent component) {
		data.remove(component);
		fireTableDataChanged();
	}

	/**
	 * Return the definitions for all components in the model. 
	 * This method returns the Java code required to define all components defined in the model.
	 * 
	 * @return a String containing the Java code defining all components in the model.
	 */
	public String getDefinitions() {
		StringBuilder sb = new StringBuilder();
		for (BaseComponent c : data) {
			sb.append(c.getDefinition());
		}
		return sb.toString();
	}

	/**
	 * Return the layout code for all components in the model.
	 * This method returns the Java code for layout out all the components defined in the model.
	 * 
	 * @return a String containing the Java code laying out all the components in the model.
	 */
	public String getLayoutCode() {
		StringBuilder sb = new StringBuilder();
		for (BaseComponent c : data) {
			sb.append(c.getLayoutCode());
		}
		return sb.toString();
	}
	
	/**
	 * Move the component on the given row one row down (if possible)
	 * 
	 * @param idx the row to move down
	 */
	public void moveComponentDown(int idx) {
		if (idx<data.size()-1) {
			BaseComponent c = data.get(idx);
			data.set(idx, data.get(idx+1));
			data.set(idx+1, c);
			fireTableRowsUpdated(idx, idx+1);
		}
	}

	/**
	 * Move the component on the given row one row up (if possible)
	 * 
	 * @param idx the row to move up
	 */
	public void moveComponentUp(int idx) {
		if (idx>0) {
			BaseComponent c = data.get(idx);
			data.set(idx, data.get(idx-1));
			data.set(idx-1, c);
			fireTableRowsUpdated(idx-1, idx);
		}
	}

	/**
	 * Save content of model to file.
	 * Saves the content of this model to the given file.
	 * 
	 * @param os the OutputStream to save the content of the model to 
	 */
	public void save(OutputStream os) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(data);
		} catch (IOException e) {
			logger.log(Level.OFF, "Error saving file", e);
		}		
	}

	/**
	 * Read a model from file.
	 * Clears the current model and reads a new model from the given stream.
	 * 
	 * @param is the stream to read the model from.
	 */
	@SuppressWarnings("unchecked")
	public void load(InputStream is) {
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(is);
			data = (ArrayList<BaseComponent>) ois.readObject();
			fireTableDataChanged();
		} catch (IOException e) {
			logger.log(Level.OFF, "Error loading file", e);
		} catch (ClassNotFoundException e) {
			logger.log(Level.FINE, "Error loading file, class cast exception", e);
		}
	}

	/**
	 * Clears the model, remove all components.
	 */
	public void clear() {
		data.clear();
		fireTableDataChanged();
	}

}
