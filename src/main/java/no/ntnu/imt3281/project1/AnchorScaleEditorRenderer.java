package no.ntnu.imt3281.project1;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Used to render fields in the editor for the anchor column in the GBLE table
 * @author oivindk
 *
 */
@SuppressWarnings("serial")
public class AnchorScaleEditorRenderer extends JLabel implements ListCellRenderer<Integer> {
	private Map<Integer, String> aligns = new TreeMap<>();
	private Map<Integer, String> scale = new TreeMap<>();
	private Map<Integer, String> totalMap = new TreeMap<>();
	
	/**
	 * @see javax.swing.ListCellRenderer
	 */
	public AnchorScaleEditorRenderer() {
		aligns.put(GridBagConstraints.CENTER, "anchor_center");
		aligns.put(GridBagConstraints.NORTH, "anchor_north");
		aligns.put(GridBagConstraints.NORTHWEST, "anchor_northwest");
		aligns.put(GridBagConstraints.WEST, "anchor_west");
		aligns.put(GridBagConstraints.SOUTHWEST, "anchor_southwest");
		aligns.put(GridBagConstraints.SOUTH, "anchor_south");
		aligns.put(GridBagConstraints.SOUTHEAST, "anchor_southeast");
		aligns.put(GridBagConstraints.EAST, "anchor_east");
		aligns.put(GridBagConstraints.NORTHEAST, "anchor_northeast");
		scale.put(GridBagConstraints.NONE, "scale_none");
		scale.put(GridBagConstraints.HORIZONTAL, "scale_horizontal");
		scale.put(GridBagConstraints.VERTICAL, "scale_vertical");
		scale.put(GridBagConstraints.BOTH, "scale_both");
		totalMap = Stream.of(aligns, scale).map(Map::entrySet).flatMap(Collection::stream).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		setOpaque(true);
	}
	
	/**
	 * Return the values used for representing CENTER, NORTH etc anchoring
	 * 
	 * @return numbers between 10 and 18 (at least as of Java 1.8)
	 */
	public Integer[] getAlignValues() {
		return aligns.keySet().toArray(new Integer[9]);
	}
	
	/**
	 * Return the values used for representing NONE, HORIZONTAL, VERTICAL and BOTH scaling values
	 * 
	 * @return numbers between 0 - 3 (at least as of Java 1.8)
	 */
	public Integer[] getScaleValues() {
		return scale.keySet().toArray(new Integer[4]);
	}
	
	/**
	 * Return the map used to convert between number and string for anchor/scale values
	 * @return
	 */
	public Map<Integer, String> getMap() {
		return totalMap;
	}

	/**
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(JList, Object, int, boolean, boolean)
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends Integer> list, Integer value, int index,
			boolean isSelected, boolean cellHasFocus) {
		if (isSelected) {
			setBackground(list.getSelectionBackground());
		} else {
			setBackground(list.getBackground());
		}
		if (value!=null) {
			setIcon(new ImageIcon(getClass().getResource("/icons/"+getMap().get(value)+".png")));
		}
		return this;
	}
}
