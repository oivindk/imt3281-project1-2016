package no.ntnu.imt3281.project1;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.ntnu.imt3281.I18N.I18N;

/**
 * Objects of this class will represent JTextAreas in the layout created by the editor
 * 
 * @author �ivind Kolloen
 *
 */
public class TextArea extends BaseComponent {

	/**
	 * Update when adding/removing fields from this class, used for saving/loading to file.
	 */
	private static final long serialVersionUID = 1L;
	private int textWidth=0;
	private int textHeight=0;
	private boolean wrap=false;
	transient Logger logger = Logger.getLogger(getClass().getName());

	/**
	 * Create a new text area based on another component.
	 * 
	 * @see BaseComponent#BaseComponent(BaseComponent)
	 * 
	 * @param component base the new button on this component
	 */
	public TextArea(BaseComponent component) {
		super(component);
	}

	/**
	 * Create a new text area.
	 * 
	 * @see BaseComponent#BaseComponent()
	 */
	public TextArea() {
		super();
	}
	
	/**
	 * Return a JDialog used to change the number of columns, rows and text wrap of this text area.
	 * @see BaseComponent#getSpecialEditor()
	 *  
	 * @return a JDialog that enables the user to change the desired fields
	 */
	@Override
	public Component getSpecialEditor() {
		JPanel editor = new JPanel ();
		editor.setLayout(new GridLayout(2, 1));
		GridBagLayout layout = new GridBagLayout();
		editor.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.EAST;
		JLabel widthLabel = new JLabel(I18N.getString("textArea.editor.width.label"));
		editor.add(widthLabel, gbc);
		JTextField width = new JTextField(Integer.toString(textWidth), 10);
		width.setToolTipText(I18N.getString("textArea.editor.width.tooltip"));
		gbc.gridx = 2;
		gbc.anchor = GridBagConstraints.WEST;
		editor.add(width, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.anchor = GridBagConstraints.EAST;
		JLabel heightLabel = new JLabel(I18N.getString("textArea.editor.height.label"));
		editor.add(heightLabel, gbc);
		JTextField height = new JTextField(Integer.toString(textHeight), 10);
		height.setToolTipText(I18N.getString("textArea.editor.height.tooltip"));
		gbc.gridx = 2;
		gbc.anchor = GridBagConstraints.WEST;
		editor.add(height, gbc);
		
		gbc.gridx = 1;
		gbc.gridwidth = 2;
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.WEST;
		JCheckBox wrapCheckBox = new JCheckBox(I18N.getString("textArea.editor.wrap.label"), wrap);
		wrapCheckBox.setToolTipText(I18N.getString("textArea.editor.wrap.tooltip"));
		editor.add(wrapCheckBox, gbc);
		
		gbc.gridwidth = 1;
		gbc.gridy = 4;
		gbc.anchor = GridBagConstraints.EAST;
		JButton ok = new JButton(I18N.getString("button.ok"));
		editor.add(ok, gbc);
		gbc.gridx = 2;
		gbc.anchor = GridBagConstraints.WEST;
		JButton cancel = new JButton(I18N.getString("button.cancel"));
		editor.add(cancel, gbc);
		
		ok.addActionListener(e->{
			try {
				textWidth = Integer.parseInt(width.getText());
				textHeight = Integer.parseInt(height.getText());
				wrap = wrapCheckBox.isSelected();
			} catch (RuntimeException re) {
				logger.log(Level.OFF, "User input error", re);
				JOptionPane.showMessageDialog(null, I18N.getString("error.textarea.editor.notANumber"));
				return;
			}
			closeEditor(editor);
		});
		
		cancel.addActionListener(e->{
			closeEditor(editor);
		});
		
		return editor;

	}

	/**
	 * @param editor
	 */
	private void closeEditor(JPanel editor) {
		Component c = editor;
		do {
			c = c.getParent();
		} while (!(c instanceof JDialog));
		((JDialog)c).setVisible(false);
		((JDialog)c).dispose();
	}

	/**
	 * Set the width of this text area, that is, how many characters wide it should be. 
	 * @see javax.swing.JTextArea#setColumns(int)
	 * 
	 * @param width the number of characters this text field should have room for.
	 */
	public void setTextRows(int width) {
		this.textHeight = width;
	}

	/**
	 * Set the height of this text area, that is, how many lines is should be. 
	 * @see javax.swing.JTextArea#setRows(int)
 	 * 
	 * @param height the number of lines this text Area should have room for.
	 */
	public void setTextCols(int height) {
		this.textWidth = height;
	}

	/**
	 * Set the line wrap property of this text area.  
	 * @see javax.swing.JTextArea#setLineWrap(boolean)
 	 * 
	 * @param wrap sets the line-wrapping policy of the text area.
	 */
	public void setWrap(boolean wrap) {
		this.wrap = wrap;
	}

	/**
	 * Used to get the definition of the JTextField.
	 * This code will be placed just below the class definition of the generated code.
	 * 
	 * @return a String with the Java code for creating this text field as a property in the class.
	 */
	@Override
	public String getDefinition() {
		StringBuilder sb = new StringBuilder();
		sb.append("\tJTextArea ");
		sb.append(getVariableName());
		sb.append(" = new JTextArea(\"");
		sb.append(getText());
		sb.append("\"");
		if (textHeight>0||textWidth>0) {
			sb.append(", ");
			sb.append(textHeight);
			sb.append(", ");
			sb.append(textWidth);
		}
		sb.append(");\n");
		return sb.toString();
	}

	/**
	 * Return the Java code to be used to place this component in the layout.
	 * This is the code to placed in the constructor of the class that will be created for the complete layout.
	 * 
	 * @return a String with the Java code handling the layout for this component.
	 */
	@Override
	public String getLayoutCode() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.getLayoutCode());
		if (wrap) {
			sb.append("\t\t");
			sb.append(getVariableName());
			sb.append(".setWrapStyleWord(true);\n");
		}
		return sb.toString();
	}
}
