package no.ntnu.imt3281.project1;

/**
 * Objects of this class will represent JButtons in the layout created by the editor
 * 
 * @author Øivind Kolloen
 *
 */
public class Button extends BaseComponent {

	/**
	 * Update when adding/removing fields from this class, used for saving/loading to file.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see BaseComponent#BaseComponent(BaseComponent)
	 * @param component base the new button on this component
	 */
	public Button(BaseComponent component) {
		super(component);
	}

	/**
	 * @see BaseComponent#BaseComponent()
	 */
	public Button() {
		super();
	}

	/**
	 * Used to get the definition of the JButton.
	 * this code will be placed just below the class definition of the generated code.
	 * 
	 * @return a String with the Java code for creating this button as a property in the class.
	 */
	@Override
	public String getDefinition() {
		return "\tJButton "+getVariableName()+" = new JButton(\""+getText()+"\");\n";
	}

}
