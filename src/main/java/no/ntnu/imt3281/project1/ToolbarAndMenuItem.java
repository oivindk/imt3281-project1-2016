package no.ntnu.imt3281.project1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

import no.ntnu.imt3281.I18N.I18N;

public class ToolbarAndMenuItem {
	ActionListener al = null;
	AbstractAction aa;
	Logger logger = Logger.getLogger(getClass().getName());
	
	@SuppressWarnings("serial")
	public ToolbarAndMenuItem(ActionListener al, String iconName, String nameKey) {
		this.al = al;
		ImageIcon fileNewIcon = null;
    	try {
    		if (iconName!=null) {
    			fileNewIcon = new ImageIcon (ImageIO.read(getClass().getResource(iconName)));
    		}
    	} catch (IOException ioe) {
    		logger.log(Level.SEVERE, "Toolbar icon not found", ioe);
    	}
		aa = new AbstractAction(I18N.getString(nameKey), fileNewIcon) {
			@Override
			public void actionPerformed(ActionEvent e) {
				ToolbarAndMenuItem.this.al.actionPerformed(e);				
			}
		};
		aa.putValue(Action.SHORT_DESCRIPTION, I18N.getString(nameKey+".tooltip"));
	}

	public JMenuItem getJMenuItem() {
		JMenuItem mi = new JMenuItem(aa);
		mi.setIcon(null);
		return mi;
	}
	
	public AbstractAction getAbstractAction() {
		return aa;
	}
}
