package no.ntnu.imt3281.help;

import java.awt.GridLayout;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.ntnu.imt3281.I18N.I18N;

@SuppressWarnings("serial")
public class Help extends JFrame {
	JEditorPane helpViewer = new JEditorPane();
	transient Logger logger = Logger.getLogger(getClass().getName());
	
	public Help() {
		super(I18N.getString("helpWindow.title"));
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 1, 10, 10));
		p.add(new JScrollPane(helpViewer));
		add (p);
		helpViewer.addHyperlinkListener(e->{
			try {
				helpViewer.setPage(e.getURL());
			} catch (IOException e1) {
				logger.log(Level.WARNING, "Unable to find help page: "+e.getURL().getFile() , e1);
			}
		});
		setSize(500,400);
		setLocation(100, 100);
	}
	
	public void displayPage (String page) {
		try {
			helpViewer.setPage(getClass().getResource("/no/ntnu/imt3281/help/"+page));
		} catch (IOException e) {
			logger.log(Level.WARNING, "Unable to find help page: "+page, e);
		}
		setVisible(true);
	}
}
